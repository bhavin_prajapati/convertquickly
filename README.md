## How to Use

### Dependencies

| Dependency                                | Installation                                 |
|:------------------------------------------|:---------------------------------------------|
| [Node.js](http://nodejs.org/)             | [download](http://nodejs.org/download/)      |
| [MongoDB](http://www.mongodb.org/)        | [download](http://www.mongodb.org/downloads) |
| [Redis](http://redis.io/)                 | [download](http://redis.io/download)         |
| [Bower](http://twitter.github.com/bower/) | `npm install bower -g`                       |
| [Compass](http://compass-style.org/)      | `gem install compass`                        |
| [Grunt](http://gruntjs.com/)              | `npm install grunt-cli -g`                   |

  Make sure `MongoDB` server is running somewhere (or use free services such as [MongoHQ](https://www.mongohq.com/) or [MongoLab](https://mongolab.com/)). Update configuration information in `config/{development,heroku,production}.json`.

  `Redis` server is optional but it is highly recommended. Modify *session.store._use* variable as well as Redis connection information in the configuration file if you wish to use Redis as session backend. There is also a free Redis hosting provider, [Redis To Go](http://redistogo.com/).

### Installation

  Download `convertquickly` and install dependency modules:

    $ git clone https://bhavin_prajapati@bitbucket.org/bhavin_prajapati/convertquickly.git convertquickly
    $ cd convertquickly
    $ npm install && bower install

### Development (`config/development.json`)

  [Grunt](http://gruntjs.com/) tasks to build your app:

    $ grunt server             # start server
    $ grunt                    # jshint & build
    $ grunt clean              # clean grenerated files

## Deployment

### Production Server (`config/production.json`)

  First, prepare and optimize all files used in production environment:

    $ grunt

  Then your app can be started in production mode with a port using this command:

    $ ./start production 3000