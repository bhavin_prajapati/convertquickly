/*
 * client/js/views/home/layout.js
 */

/* global define */

define([
  'backbone',
  'collections/content',
  'views/home/content',
  'hbs!templates/home/layout'
], function (Backbone, Content, ContentView, layoutTpl) {
  'use strict';

  return Backbone.Marionette.Layout.extend({
    template: layoutTpl,

    regions: {
      mainRegion: '#layout'
    },

    onShow: function () {
      var content = new Content();
      this.mainRegion.show(new ContentView({
        collection: content
      }));
    }
  });

});
