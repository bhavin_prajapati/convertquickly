/*
 * server/routes.js
 */

'use strict';

var ultimate = require('ultimate');

exports.register = function (app) {
  var csrf = ultimate.server.controller.csrf,
      ensureAdmin = ultimate.server.controller.ensureAdmin,
      ensureGuest = ultimate.server.controller.ensureGuest,
      ensureUser = ultimate.server.controller.ensureUser;

  var error404 = ultimate.server.route.error404,
      restify = ultimate.server.route.restify;

  var c = app.controllers,
      s = app.servers.express.getServer();

  // Pages
  s.get('/about', c.home.about);
  s.get('/advertisers', c.home.advertisers);
  s.get('/jobs', c.home.jobs);
  s.get('/contact', c.home.contact);
  s.get('/suppliers', c.home.suppliers);

  // Account
  s.get('/account', ensureUser, c.account.index);

  // Purchase
  s.get('/purchase', ensureUser, c.purchase.index);

  // API
  restify.any(s, '/stripe', c.stripe, ['list', 'post']);

  // Admin
  s.get('/admin', ensureAdmin, c.admin.index);

  // Auth
  s.get('/login', ensureGuest, c.auth.login);
  s.post('/login', ensureGuest, csrf, c.auth.loginPOST);
  s.get('/logout', c.auth.logout);
  s.post('/logout', c.auth.logoutPOST);
  s.get('/lost-password', ensureGuest, c.auth.lostPassword);
  s.post('/lost-password', ensureGuest, csrf, c.auth.lostPasswordPOST);
  s.get('/register', ensureGuest, c.auth.register);
  s.post('/register', ensureGuest, csrf, c.auth.registerPOST);
  s.get('/auth/facebook', c.auth.facebook);
  s.get('/auth/facebook/callback', c.auth.facebookCallback);
  s.get('/auth/facebook/success', c.auth.facebookSuccess);
  s.get('/auth/google', c.auth.google);
  s.get('/auth/google/callback', c.auth.googleCallback);
  s.get('/auth/google/success', c.auth.googleSuccess);
  s.get('/auth/twitter', c.auth.twitter);
  s.get('/auth/twitter/callback', c.auth.twitterCallback);
  s.get('/auth/twitter/success', c.auth.twitterSuccess);

  // Extra routes
  error404.register(s);
};
