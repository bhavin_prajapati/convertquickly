/*
 * server/models/ListItem.js
 */

'use strict';

var _ = require('lodash'),
	mongoose = require('mongoose');

var plugin = mongoose.customPlugin;

// Schema
var schema = new mongoose.Schema({
	hashtags: [{ type: String }],
	emoticons: [{
		name: { type: String, required: true },
		count: { type: Number, required: true }
	}],
	title: { type: String, required: true },
	description: { type: String, required: true },
	url: { type: String, required: true },
});

schema.methods.emoticonImg = function () {
	var topEmoticon = '';
	var top = 0;
	_.each(this.emoticons, function (emoticon) {
		if(emoticon.count >= top) {
			top = emoticon.count;
			topEmoticon = emoticon.name;
		}
	});
	var emoticonimg = '<img src="/img/emoticons/' + topEmoticon + '.png">';
	return emoticonimg;
};

schema.methods.hashtagsLinks = function () {
	var hashtaglinks = 'hashtags: ';
	_.each(this.hashtags, function (hashtag) {
		hashtaglinks = hashtaglinks + '<a href="/search?q=' + hashtag + '">#' + hashtag + '</a>';
	});
	return hashtaglinks;
};

// Indexes
schema.path('title').index({ unique: true });

// Plugins
schema.plugin(plugin.findOrCreate);
schema.plugin(plugin.timestamps);

// ListItem
var model = mongoose.model('ListItem', schema);

// Public API
exports = module.exports = model;
