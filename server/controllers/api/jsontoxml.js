/*
 * server/controllers/api/top.js
 */

'use strict';

//var fs = require('fs'),
//    path = require('path');
var http = require('http');
var jsonxml = require('jsontoxml');
//var app = require('../../app');

function LIST(req, cb) {
  var url = req.param('url');
  var body = '';
  http.get(url, function(resp){
    resp.setEncoding('utf8');
    resp.on('data', function(chunk){
      body += chunk;
    });
    resp.on('end', function(){
      var xml = jsonxml(body);
      cb(null,xml,{'Content-Type': 'application/xml'});
    });
  }).on('error', function(e){
    console.log('Got error: ' + e.message);
  });
}

function GET(req, id, cb) {
  var url = req.param('url');
  var body = '';
  http.get(url, function(resp){
    resp.setEncoding('utf8');
    resp.on('data', function(chunk){
      body += chunk;
    });
    resp.on('end', function(){
      var xml = jsonxml(body);
      cb(null,xml,{'Content-Type': 'application/xml'});
    });
  }).on('error', function(e){
    console.log('Got error: ' + e.message);
  });
}

function POST(req, cb) {
  var xml = jsonxml(req.body);
  cb(null,xml,{'Content-Type': 'application/xml'});
}

function PUT(req, id, cb) {
  cb('invalid method - ' + req.method);
}

function DELETE(req, id, cb) {
  cb('invalid method - ' + req.method);
}

// Public API
exports.__filename = __filename;
exports.LIST = LIST;
exports.GET = GET;
exports.POST = POST;
exports.PUT = PUT;
exports.DELETE = DELETE;
