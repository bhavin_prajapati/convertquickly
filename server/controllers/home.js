/*
 * server/controllers/home.js
 */

'use strict';

function about(req, res) {
  res.render('home/about', {
    hello: 'world'
  });
}

function advertisers(req, res) {
  res.render('home/advertisers', {
    hello: 'world'
  });
}

function jobs(req, res) {
  res.render('home/jobs', {
    hello: 'world'
  });
}

function contact(req, res) {
  res.render('home/contact', {
    hello: 'world'
  });
}

function suppliers(req, res) {
  res.render('home/suppliers', {
    hello: 'world'
  });
}

// Public API
exports.about = about;
exports.advertisers = advertisers;
exports.jobs = jobs;
exports.contact = contact;
exports.suppliers = suppliers;
