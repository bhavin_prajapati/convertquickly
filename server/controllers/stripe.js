/*
 * server/controllers/api/top.js
 */

'use strict';

//var fs = require('fs'),
//    path = require('path');
//var http = require('http');

/*IMPORTANT: Make sure you fill in your secret API key into the stripe module.*/
var stripe = require('stripe')('sk_test_etBIlRYSPlM7OPHXuy1irChq');
var nodemailer = require('nodemailer');

function LIST(req, cb) {
  cb('invalid method - ' + req.method);
}

function GET(req, id, cb) {
  cb('invalid method - ' + req.method);
}


function sendEmail( fullname, email ){
  // Create a SMTP transport object
  var transport = nodemailer.createTransport('SMTP', {
    service: 'Gmail', // use well known service.
                        // If you are using @gmail.com address, then you don't
                        // even have to define the service name
    auth: {
        user: 'support@sendmysterygift.com',
        pass: 'Welcome2014!!'
      }
    });

  console.log('SMTP Configured');

  // Message object
  var message = {

      // sender info
      from: '"Mystery Gift" <support@sendmysterygift.com>',

      // Comma separated list of recipients
      to: '"' + fullname + '" <' + email + '>',

      // Subject of the message
      subject: 'Nodemailer is unicode friendly ✔', //

      headers: {
          'X-Laziness-level': 1000
        },

        // plaintext body
        text: 'Hello to myself!',

        // HTML body
        html:'<p><b>Hello</b> to myself <img src="cid:note@node"/></p>'+
           '<p>Here\'s a nyan cat for you as an embedded attachment:<br/><img src="cid:nyan@node"/></p>',

        // An array of attachments
        attachments:[

          // String attachment
          {
              fileName: 'notes.txt',
              contents: 'Some notes about this e-mail',
              contentType: 'text/plain' // optional, would be detected from the filename
            },

            // Binary Buffer attachment
            {
              fileName: 'image.png',
              contents: new Buffer('iVBORw0KGgoAAAANSUhEUgAAABAAAAAQAQMAAAAlPW0iAAAABlBMVEUAAAD/' +
                                   '//+l2Z/dAAAAM0lEQVR4nGP4/5/h/1+G/58ZDrAz3D/McH8yw83NDDeNGe4U' +
                                   'g9C9zwz3gVLMDA/A6P9/AFGGFyjOXZtQAAAAAElFTkSuQmCC', 'base64'),

              cid: 'note@node' // should be as unique as possible
            },

            // File Stream attachment
            {
              fileName: 'nyan cat ✔.gif',
              filePath: __dirname+'/nyan.gif',
              cid: 'nyan@node' // should be as unique as possible
            }
          ]
        };

  console.log('Sending Mail');
  transport.sendMail(message, function(error){
      if(error){
        console.log('Error occured');
        console.log(error.message);
        return;
      }
      console.log('Message sent successfully!');

      // if you don't want to use this transport object anymore, uncomment following line
      //transport.close(); // close the connection pool
    });
}

function POST(request, cb) {
  //Object POSTed to server contains the Stripe token from Stripe's servers.
  var transaction = request.body;
  var stripeToken = transaction.stripeToken;
  var amount = transaction.amount;
  //For now, charge $10.00 to the customer.
  var charge =
  {
    amount: amount*100, //Charge is in cents
    currency: 'USD',
    card: stripeToken //Card can either be a Stripe token, or an object containing credit card properties.
  };

  stripe.charges.create(charge,
      //All stripe module functions take a callback, consisting of two params:
      // the error, then the response.
      function(err, charge)
      {
        if(err)
        {
          console.log(err);
        }
        else
        {
          //res.json(charge);
          var res = cb(null,'getres');

          if(charge.paid) {
            console.log("charge.paid = true");
            sendEmail('Bhavin Prajapati', 'bhavin_2@hotmail.com');
            res.render('purchase/index', {
              confirm: 'true'
            });
          } else {
            console.log("charge.paid = false");
            res.render('purchase/index', {
              confirm: 'false'
            });
          }

          //cb(null, charge, {'Content-Type': 'application/json'});

          console.log('Successful charge sent to Stripe!');
        }
      }
  );
}

function PUT(req, id, cb) {
  cb('invalid method - ' + req.method);
}

function DELETE(req, id, cb) {
  cb('invalid method - ' + req.method);
}

// Public API
exports.__filename = __filename;
exports.LIST = LIST;
exports.GET = GET;
exports.POST = POST;
exports.PUT = PUT;
exports.DELETE = DELETE;
